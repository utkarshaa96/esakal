<?php
include("header.php");
?>
<html>
<head>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script  src="http://code.jquery.com/jquery-1.9.1.min.js" ></script>
    <script  src="function.js" ></script>
    <script src="https://www.w3schools.com/lib/w3.js"></script>
	<link rel="stylesheet" href="main.css">
	<title>eSakal | Not Connected User List</title>
</head>
    <body>
        <!-- Not Connected Starts Here-->
            <div class="container">
            <div style="float:right;">
                <form action="excel.php?status=notConnected" method="post">
                    <button class="btn btn-success">Export
                        <input  type="hidden" name="x">
                    </button>
                </form>
            </div>
                 <!-- Search Bar starts Here -->
                <input class="form-control" id="myInput" type="text" placeholder="Search.."><br>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table id="myTable2" class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                <th>Contact No.</th>
                                <th>Pincode</th>
                                <th>Status</th>
                                <th>Not-Connected Reason</th>
                                <th>Timestamp</th>
                                <th>Add Information</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                <?php
                                if($fornoconnected!=null){
                                    if($fornoconnected->num_rows > 0){ 
                                        while($row = $fornoconnected->fetch_assoc()){ ?>                
                                            <tr>
                                            <td><?php echo $row['contactNo']; ?></td>
                                            <td><?php echo $row['pincode']; ?></td>
                                            <td><?php echo $row['status']; ?></td>
                                            <td><?php echo $row['notCReason']; ?></td>
                                            <td><?php echo $row['timesstamp']; ?></td>
                                            <td><button class="btn btn-primary btn-sm"id="Add" data-toggle="modal" data-target="#myModal" onclick='showDiv(<?php echo $row['contactNo']; echo ",";?>"<?php echo $row['pincode'];?>")'>Change Status</button>&nbsp;&nbsp;
                                            </tr>
                                        <?php } 
                                    }else{ ?>
                                    <tr><td colspan="8">No details found.....</td></tr>
                                    <?php }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

<!-- Change status starts here -->
<!-- Popup for Add Button -->
<div id="myModal" class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Reader's Information</h4>
                <button type="button" class="close" data-dismiss="modal" role="dialog">&times;</button>
            </div>
            <div class="modal-body" id="formdiv">
            <!-- Form starts here -->
            <form action="notConnected.php" method="post">
                <!-- <div id="header" style="text-align='center'">
                    <h3><kbd>Reader's Information</kbd></h3>
                </div> -->
            <div style="text-align:left;">
                <div class="dropdown" style="float:right;">
                    <select class="selectStatus" name="select" id="select" required>
                        <option value="" disabled selected>Status</option>
                        <option value="connected">Connected</option>
                        <option value="notConnected">Not Connected</option>
                        <option value="callBack" >Call Back</option>
                        <option value="notInterested" >Not Interested</option>
                    </select>
                </div>
                <div class="form-group">
                    <b>Contact No.:  </b><b id="cn"></b>
                    <b id="pc">Pincode:  </b><b id="pn"></b>
                </div>
                <!-- Hidden Container Starts Here -->
                <!-- Div for Not interested -->
                <div class="hide1" id="notInterestedDiv" required>
                    Reason for Not-Interested:<span class="must">*</span>
                    <br>
                    <textarea class="form-control" id="notIReason" name="notIReason" rows="3" placeholder="Enter Reason Here..."></textarea>
                </div>
                <!-- Div For Not connected -->
                <div class="hide1" id="notConnectedDiv">
                    Reason for Not Connected :<span class="must">*</span>
                    <select class="custom-select" id="notCReason" name="notCReason" required>
                        <option value="" disabled selected>Please select</option>
                        <option value="Not Reachable">Not Reachable</option>
                        <option value="Not Recieving">Not Recieving</option>
                        <option value="Switched Off">Switched Off</option>
                        <option value="Busy">Busy</option>
                        <option value="Others">Others</option>
                    </select>
                    <!-- Others Div Starts Here -->
                    <div class="hide1" id="Others" >
                        <br>
                        <textarea class="form-control" name="othersReason" aria-label="Others" Placeholder="Enter Reason Here"></textarea>           
                        <span class="must">*</span>
                    </div>
                </div>
                <!-- Div for callback -->
                <div class="hide1" id="callBackDiv">
                <!-- <form action="pending.php" method="post">         -->
                    <b>Enter Date</b><span class="must">*</span>
                    <input type="date" name="callbackdate" id="callbackdate" placeholder="Enter Date" class="form-control">
                    <b>Enter Time</b><span class="must">*</span>
                    <input type="time" name="callbacktime" id="callbacktime" class="form-control">
                <!-- </form> -->
                </div>
                <!-- Div for Connected -->
                <input type="hidden" name="contactNo" id="contact" value=""> 
                <div class="hide1" id="connect">
                    <div class="form-group row" >
                        <div class="col-sm-6" id="pinCodeInput">
                            <b>PinCode</b><span class="must">*</span>
                            <input type="text" name="pin" id="pincode"  placeholder="Enter PinCode" class="form-control" required>
                        </div>
                        <div class="col-sm-6">
                            <b>Name</b><span class="must">*</span>
                            <input type="text" name="name" id="name" placeholder="Enter name" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <b>City</b><span class="must">*</span>
                            <input type="text" name="city" id="city" placeholder="Enter city" class="form-control" required>
                        </div>
                        <div class="col-sm-6">
                            <b>Address</b><span class="must">*</span>
                            <input type="text" name="address" id="address" placeholder="Enter Address" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6"><span class="must">*</span>
                            <b>Last Newspaper read</b>
                            <input type="text" name="lastNewspaper" id="lastnewspaper" placeholder="Enter newspaper" class="form-control" required>
                        </div>
                        <div class="col-sm-6">
                            <b>Start Date of Subscription</b><span class="must">*</span>
                            <input type="date" name="startDate" id="startDate" placeholder="Enter start date of subscription" class="form-control">
                        </div>
                    </div>
                </div> <br>
                <!-- Hidden Container Ends here -->
                <div class="form-group">
                    <button class="btn btn-primary btn-sm" type="submit" id="submit" >Submit</button>
                    <!-- <button class="btn btn-primary btn-sm" type="button">Cancel</button> -->
                </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    </div>
</body>
</html>
