<?php
include("dbconfig.php");
include("Main.php");
include("record.php");
include("callBackPhp.php");
include("vendor.php");
include("session.php");
?>
<!DOCTYPE html>
<html>
<head>
    <script src="https://www.w3schools.com/lib/w3.js"></script> 
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="shortcut icon" href="http://www.esakal.com/sites/esakal/themes/smg980/favicon.ico" type="image/vnd.microsoft.icon">	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="main.css">
</head>
<body>
    <!-- Navigation Bar Starts Here -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#" style="color: white">
                    <img src="http://www.esakal.com/sites/esakal/themes/smg980/logo.png" width="200" height="50" class="d-inline-block align-top" alt="">
                </a>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="home.php"style="color: white">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">Subscription Requests<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="pending.php" >Pending </a></li>
                            <li><a href="callBack.php">CallBack </a></li>
                            <li><a href="notConnected.php">Not-Connected </a></li>
                            <li><a href="connected.php">Connected </a></li>
                            <li><a href="subscriptionStatus.php">Subscription Status </a></li>
                        </ul>           
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">Report<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="userList.php">Connected Users</a></li>
                            <li><a href="excel.php">Export as Excel</a></li>
                        </ul>           
                    </li>
                </ul>
            </div>
            <button class="btn btn-danger navbar-btn" type="submit" onclick="location.href='logout.php'">Logout</button>
        </div>
    </nav>    
    <br>
</body>
</html>