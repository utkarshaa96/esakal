<?php
include("header.php");
?>
<html>
<head>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script  src="http://code.jquery.com/jquery-1.9.1.min.js" ></script>
    <script  src="function.js" ></script>
    <script src="https://www.w3schools.com/lib/w3.js"></script>
	<link rel="stylesheet" href="main.css">
    <title>eSakal | Connected User List</title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
        $( "#mydiv" ).draggable();
    } );
    
    function setVendor(vendorStatus) {
        $.ajax({
            type: "GET",
            url: "vendor.php",
            data: { vendorStatus: vendorStatus },
            success: function(result) {
                console.log(result);
            },
            error: function() {
                return false;
            }
        })
    }
    function getHawker() {
        var pincode = $("#pincode2").val();
        // var vendorName = $("#vendor_Name").text();
        // alert(pincode);
        $.ajax({
            type: "GET",
            url: "getHawker.php",
            data: { pincode: pincode },
            success: function(result) {
                console.log(result);
                $("#vendorNameDiv").html(result);
                // $("#vendorContactDiv").html(result);
            },
            error: function() {
                return false;
            }
        })
    }
    </script>
</head>
    <body>
        <!-- Connected Starts Here-->
        <div class="container">
            <div style="float:right;">
                <form action="excel.php?status=connected" method="post">
                    <button class="btn btn-success">Export
                        <input  type="hidden" name="x">
                    </button>
                </form>
            </div>
            <!-- <br> -->
                <!-- Search Bar starts Here -->
            <input class="form-control" id="myInput" type="text" placeholder="Search.."><br>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="myTable2" class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                            <th>Status</th>
                            <th>Contact No.</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Pincode</th>
                            <th>Timestamp</th>
                            <th>Add Information</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            <?php
                            if($forconnected!=null){
                                if($forconnected->num_rows > 0){ 
                                    while($row = $forconnected->fetch_assoc()){ ?>                
                                        <tr>
                                        <td><?php echo $row['status']; ?></td>
                                        <td><?php echo $row['contactNo']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        <td><?php echo $row['address']; ?></td>
                                        <td><?php echo $row['city']; ?></td>
                                        <td><?php echo $row['pincode']; ?></td>
                                        <td><?php echo $row['timesstamp']; ?></td>
                                        <td><button class="btn btn-primary btn-sm"id="Add" data-toggle="modal" data-target="#myModal" onclick='showvendor(<?php echo $row['contactNo']; echo ",";?>"<?php echo $row['name'];?>"<?php echo ","; echo $row['pincode']; echo ",";?>"<?php echo $row['city'];?>"<?php echo",";?>"<?php echo $row['address'];?>")';>Change Vendor Status</button></td>
                                        </tr>
                                    <?php } 
                                }else{ ?>
                                <tr><td colspan="8">No details found.....</td></tr>
                                <?php }
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <!-- Draggable Div Starts Here -->
        <div id="mydiv" draggable="true">
            <div id="mydivheader">User's Information</div>
            <b class="text">Contact No.:</b><label name="contactNo2" id="contact"></label><br>
            <b class="text">Name: </b><label id="name"></label><br>
            <b class="text">City: </b><label id="city"></label><br>
            <b class="text">Address: </b><label id="address"></label><br>
            <b class="text">Pincode: </b><label id="pincode"></label>
        </div>
    
        <div class="modal fade" id="myModal" id="formdiv2" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Vendor's Information</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="connected.php" method="post">
                        <input type="hidden" id="contactNo2" name="contactNo2">
                        <input type="hidden" id="pincode2" name="pincode2">
                        <h5>Vendor's Information</h5>
                        <div style="text-align:left;">
                        <p style="display:inline-flex;">Select Status :</p>
                            <div class="dropdown" style="float:right;">
                                <select name="vendorStatus" class="vendorStatus custom-select" onchange="" required>
                                    <option value="" disabled selected>Select Status</option>
                                    <option value="Vendor Identified">Vendor Identified</option>
                                    <!-- <option value="Vendor Contacted">Vendor Contacted</option> -->
                                    <!-- <option value="Subscription Started" >Subscription Started</option> -->
                                    <!-- <option value="Subscription Not Started" >Subscription Not Started</option> -->
                                </select>
                            </div>

                        <!-- Vendor Name Div Starts Here -->
                        <div class="hide1" id="vendorNameDiv" name="vendorNameDiv">
                            <b>Vendor's Name</b><span class="must">*</span>
                            <select id="vendorName" name="vendorName" class="custom-select" onchange="">
                                <option value="Vendor Name" disable selected>Select Vendor Name</option>
                            </select>
                        </div>
                        
                        <!-- Vendor Contact Div Starts Here -->
                        <!-- <div class="hide1" id="vendorContactDiv" name="vendorContactDiv"> 
                        </div> -->
                        <!-- Subscription Not Started Div Starts Here -->
                        <!-- <div class="hide1" id="laterReasonDiv">
                            Reason for Not Starting the Subscription<span class="must">*</span>
                            <br>
                            <textarea class="form-control" name="laterReason" Placeholder="Enter Reason Here..."></textarea>           
                        </div> -->
                        <br><button class="btn btn-primary btn-sm" type="submit" id="submit">Submit</button>
                        <!-- <button class="btn btn-primary btn-sm" type="button" onclick="location.href='connected.php'">Cancel</button> -->
                    </form>  
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
