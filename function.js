// alert("function.js");

// Function for contact No
function set() {
    return document.getElementById("cn").innerHTML;
}


// Main Div is Handled Here
function showDiv(x, y) {
    document.getElementById("formdiv").style.display = "block";
    document.getElementById('cn').innerHTML = x;
    // If pincode is null
    if (y == false) {
        document.getElementById('pc').style.display = "none";
        document.getElementById('pn').style.display = "none";
        document.getElementById('pinCodeInput').style.display = "block";


    } else {
        document.getElementById('pn').innerHTML = y;
        document.getElementById('pn').style.display = "block";
        document.getElementById('pc').style.display = "block";
        document.getElementById('pincode').required = false;
        document.getElementById('pinCodeInput').style.display = "none";
    }
    document.getElementById("contact").value = set();
}

// Show Vendor Starts Here
function showvendor(contactNo, name, pincode, vendorName, city, address) {
    // alert(contactNo);
    $("#contact").val(contactNo);
    // alert($("#contact").val());
    $("#name").val(name);
    // alert(name);
    // alert(pincode);
    // alert(city);
    // alert(address);
    // alert($("#name").val());
    $("#vendor_name").val(vendorName);
    alert($("#vendor_name").val());
    $("#pincode").val(pincode);
    $("#city").val(city);
    $("#address").val(address);
    $("#contactNo2").val(contactNo);
    $("#pincode2").val(pincode);
    // alert($("#pincode2").val());
}

$(document).ready(function() {
    // Others text box handled here
    $("select.custom-select").change(function() {
        var select = $(".custom-select option:selected").val();
        // alert(select);
        switch (select) {
            case "Others":
                document.getElementById("Others").style.display = "block";
                document.getElementById("formdiv").style.height = "450px";
                break;
        }
    })

    function getHawker() {
        var pincode = $("#pincode2").val();
        alert("geth");
        // alert(pincode);
        // var vendorName = $("#vendor_Name").text();
        // alert(pincode);
        $.ajax({
            type: "GET",
            url: "getHawker.php",
            data: { pincode: pincode },
            success: function(result) {
                console.log(result);
                $("#vendorName").html(result);
                // $("#vendorContactDiv").html(result);
            },
            error: function() {
                return false;
            }
        })
    }

    function getVendor() {
        var vendorName = $("#vendor_name").val();
        // alert(vendorName);
        $.ajax({
            type: "GET",
            url: "getVendor.php",
            data: { vendorName: vendorName },
            success: function(result) {
                console.log(result);
                $("#vendorContactDiv").html(result);
                // $("#vendorContactDiv").html(result);
            },
            error: function() {
                return false;
            }
        })
    }
    // Vendor Status Handled Here
    $("select.vendorStatus").change(function() {
        var vendor = $(".vendorStatus option:selected").val();
        // alert(vendor);
        switch (vendor) {
            case "Vendor Identified":
                document.getElementById("vendorNameDiv").style.display = "block";
                document.getElementById("laterReasonDiv").style.display = "none";
                document.getElementById("vendorContactDiv").style.display = "none";
                getHawker();
                break;
            case "Vendor Contacted":
                document.getElementById("vendorContactDiv").style.display = "block";
                document.getElementById("laterReasonDiv").style.display = "none";
                document.getElementById("vendorNameDiv").style.display = "none";
                getVendor();
                break;
            case "Subscription Started":
                document.getElementById("laterReasonDiv").style.display = "none";
                document.getElementById("vendorContactDiv").style.display = "none";
                document.getElementById("vendorNameDiv").style.display = "none";
                break;
            case "Subscription Not Started":
                document.getElementById("laterReasonDiv").style.display = "block";
                document.getElementById("vendorContactDiv").style.display = "none";
                document.getElementById("vendorNameDiv").style.display = "none";
                break;
        }
    })
    $("select.selectStatus").change(function() {
        var value = $(".selectStatus option:selected").val();
        // alert("function.js");
        switch (value) {
            case "Connected":
                document.getElementById("connect").style.display = "block";
                document.getElementById("notConnectedDiv").style.display = "none";
                document.getElementById("notInterestedDiv").style.display = "none";
                document.getElementById("callBackDiv").style.display = "none";
                // document.getElementById("formdiv").style.height = "505px";
                document.getElementById("notIReason").required = false;
                document.getElementById("notCReason").required = false;
                document.getElementById("othersReason").required = false;
                document.getElementById("callbackdate").required = false;
                document.getElementById("callbacktime").required = false;
                break;
            case "callBack":
                document.getElementById("callBackDiv").style.display = "block";
                document.getElementById("connect").style.display = "none";
                document.getElementById("notConnectedDiv").style.display = "none";
                document.getElementById("notInterestedDiv").style.display = "none";
                // document.getElementById("formdiv").style.height = "387px";
                document.getElementById("name").required = false;
                document.getElementById("city").required = false;
                document.getElementById("address").required = false;
                document.getElementById("lastnewspaper").required = false;
                document.getElementById("pincode").required = false;
                document.getElementById("notIReason").required = false;
                document.getElementById("othersReason").required = false;
                document.getElementById("notCReason").required = false;
                break;
            case "notConnected":
                document.getElementById("notConnectedDiv").style.display = "block";
                document.getElementById("connect").style.display = "none";
                document.getElementById("callBackDiv").style.display = "none";
                document.getElementById("notInterestedDiv").style.display = "none";
                // document.getElementById("formdiv").style.height = "330px";
                document.getElementById("name").required = false;
                document.getElementById("city").required = false;
                document.getElementById("address").required = false;
                document.getElementById("lastnewspaper").required = false;
                document.getElementById("pincode").required = false;
                document.getElementById("callbackdate").required = false;
                document.getElementById("callbacktime").required = false;
                document.getElementById("notIReason").required = false;
                break;
            case "notInterested":
                document.getElementById("notInterestedDiv").style.display = "block";
                document.getElementById("connect").style.display = "none";
                document.getElementById("notConnectedDiv").style.display = "none";
                document.getElementById("callBackDiv").style.display = "none";
                // document.getElementById("formdiv").style.height = "375px";
                document.getElementById("name").required = false;
                document.getElementById("city").required = false;
                document.getElementById("address").required = false;
                document.getElementById("lastnewspaper").required = false;
                document.getElementById("pincode").required = false;
                document.getElementById("callbackdate").required = false;
                document.getElementById("callbacktime").required = false;
                document.getElementById("othersReason").required = false;
                document.getElementById("notCReason").required = false;
                break;
            default:
                document.getElementById("connect").style.display = "none";
                // document.getElementById("formdiv").style.height = "245px";
                document.getElementById("name").required = false;
                document.getElementById("city").required = false;
                document.getElementById("address").required = false;
                document.getElementById("lastnewspaper").required = false;
        }
    });
});

// Script for search 
$(document).ready(function() {
    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});