<?php 
// Filename : main.php
// Created By : Utkarsha Yewale
// Created Date : 28/09/2018

// query for displaying connected users 
$connected = $con->query("select * from list where status = 'connected' AND vendorName IS NOT NULL");

// query for displaying connected users 
$forconnected = $con->query("select * from list where status = 'connected' AND vendorName IS NULL");

// $forVendor = $con->query("select * from list where vendorStatus IS NOT NULL"); 

// query for displaying not Interested users 
$forNotInterested = $con->query("select * from list where status = 'notInterested'");

// query for displaying not connected users 
$fornoconnected = $con->query("select * from list where status = 'notConnected'");

// query for displaying pending users 
$forpending = $con->query("select * from list where status = 'pending'");

// query for displaying callBack users 
$forcallback = $con->query("select contactNo, pincode, callbackdate, TIME_FORMAT(callbacktime, '%h:%i %p') callbacktime from list where status = 'callBack'");

?>
