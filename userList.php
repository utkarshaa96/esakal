<?
include("header.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script  src="function.js" ></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="main.css">
        <title>eSakal | Connected User List</title>
  </head>
  <body>
  <!-- Table for connected user list -->
  <div class="table-responsive table-fixed">
    <div class="container mt-3">
      <h2>Connected User Details</h2>
      <!-- Input for Search -->
      <input class="form-control" id="myInput" type="text" placeholder="Search.."><br>
      <table id="myTable" class="table table-hover table-bordered">
        <!-- Header of table -->
        <tr class="header">
        <th>Contact No.</th>
        <th>Full Name</th>
        <th>Vendor's Name</th>
        <th>Start Date of Subscription</th>
        </tr>
      <?php
      include 'dbconfig.php';       
      // SQL Query         
      $query = $con->query("select * from list where status = 'connected' AND vendorName IS NOT NULL");
			echo "<meta http-equiv='refresh' content='1;URL=". $_SERVER['PHP_SELF']."'>";			
      if($query->num_rows > 0){ 
        while($row = $query->fetch_assoc()){ ?>  
          <tbody id="myTable">              
            <tr>
              <td><?php echo $row['contactNo']; ?></td>
              <td><?php echo $row['name']; ?></td>
              <td><?php echo $row['vendorName']; ?></td>
              <td><?php echo $row['startDate']; ?></td>
            </tr>
              <?php } }else{ ?>
              <tr><td colspan="5">No details found.....</td></tr>
              <?php } 
              $con->close();?>
          </tbody>
        </table>
      </div>       
    </div>                   
  </body>
</html>
