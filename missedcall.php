<?php
// Filename : missedcall.php
// Created on:"21-9-2018" 
// Created by:"Aarti"

//Handles API Post request
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$json = file_get_contents('php://input');
	if($json !== false){
		$data = json_decode($json, true);
		if ($data !== null){  	
			$contactNo=$data["who"];
            $channelId=$data["channelId"];
            $circle=$data["circle"];
            $operator=$data["operator"];
            $qualityScore=$data["qualityScore"];
			if(isset($data["dateTime"])){
				$timestamp=$data["dateTime"];
			}
			else{
				date_default_timezone_set('Asia/Kolkata');
				$timestamp=date('Y-m-d H:i:s');
			}
		}
	}
}
//Handles API GET request
else if($_SERVER["REQUEST_METHOD"]=="GET"){	
	if($_GET["who"]){
        $contactNo=$_GET["who"];
        $channelId=$_GET["channelId"];
        $circle=$_GET["circle"];
        $operator=$_GET["operator"];
        $qualityScore=$_GET["qualityScore"];
		if(isset($_GET["dateTime"])){
			$timestamp=$_GET["dateTime"];
		}
		else{
			date_default_timezone_set('Asia/Kolkata');
			$timestamp=date('Y-m-d H:i:s');
		}
	}
}
//inserts data in database if data is received
if($contactNo != null) {
	try{
		$con=mysqli_connect("localhost","user","user@123","sakal");
		if(!$con){
            // echo"failed";
            $log  = "MissedCall API - Message: DataBase connection failed ".
				"| IP Address: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a")."\n";
            logIt($log);
			die("connection failed:".mysqli_connect_error());
		}
		else{
			// echo "Connection Done";
		} 

		$status='pending';

		$sql="INSERT INTO list(contactNo,channelId,circle,operator,qualityScore,status,timesstamp) VALUES('$contactNo','$channelId','$circle','$operator','$qualityScore','$status','$timestamp')";
		
		if ($con->query($sql) === TRUE){
			$log  = "MissedCall API - Message: Success ".
				"| DateTime: ".$timestamp.
				"| ContactNo: ".$contactNo.
				"| IP Address: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").
				"| DID: ".$channelId."\n";
    		throw new Exception($log);
			$var=new \stdClass();
			$var->Status_Code = "200 OK";
			$var->Message="Record Added";
			$myJSON = json_encode($var);
			echo $myJSON;
		}
		else{
			$log  = "MissedCall API - Message: Failed ".
				"| DateTime: ".$timestamp.
				"| ContactNo: ".$contactNo.
				"| IP Address: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").
				"| DID: ".$channelId."\n";
            throw new Exception($log);
			$var=new \stdClass();
			$var->Status_Code = "200 OK";
			$var->Message="Record Added";
			$myJSON=json_encode($var);
			echo $myJSON;
			echo "Error: " . $sql . "<br>" . $con->error;
		}

		$con->close();
        }
        catch(Exception $e){
            logIt($log);
	}
}

// Function to log success or failure
function logIt($log){
	echo $log;
    file_put_contents('./log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
}
?>
