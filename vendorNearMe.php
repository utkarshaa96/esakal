<?php
include('dbconfig.php');
?>
<!DOCTYPE html>
    <html>
    <!-- Filename : vendorNearMe.php
    Created By : Utkarsha Yewale
    Created Date : 05/10/2018 -->
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>eSakal | Vendor Near Me</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link rel="stylesheet" type="text/css" media="screen" href="main.css" /> -->
    	<link rel="stylesheet" href="vendor.css">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="shortcut icon" href="http://www.esakal.com/sites/esakal/themes/smg980/favicon.ico" type="image/vnd.microsoft.icon">    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
                
            // Start of AJAX CALL
        function getCenter() {
            var PINCODE = $("#pincode").val();
            var CENTER_NAME = $("#center").val();
            $("#printCenter").text(CENTER_NAME);
            $("#printPincode").text(PINCODE);
        // alert(CENTER_NAME);
        //code for ajax call here will pass districtname to backend and get other details like cities
        $.ajax({
            type : "GET",
            url : "getCenter.php?",
            data : {CENTER_NAME:CENTER_NAME,PINCODE:PINCODE},
            success : function(result) {
                console.log(result);
                $("#cardId").html(result);  
            },
            error : function() {
                return false;
            }
        })
    }
    // End of AJAX CALL
        // function getHawkerPincode(){
        //     var PINCODE = $("#pincode").val();
        //     var CENTER_NAME = $("#center").val();        
        //     $.ajax({
        //     type : "GET",
        //     url : "getHawkerPincode.php",
        //     data : {CENTER_NAME:CENTER_NAME,PINCODE:PINCODE},
        //     success : function(result) {
        //         console.log(result);
        //         $("#cardId").html(result);
                
        //     },
        //     error : function() {
        //         return false;
        //     }    
        //     })
        // }
        </script>
        <style>
            
        </style>
    </head>

    <body>
        <nav class="navbar">
            <div class="navbarHeader">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img src="http://www.esakal.com/sites/esakal/themes/smg980/logo.png" width="200" height="50" class="d-inline-block align-top" alt="">
                    </a>
                </div>
            </div>
        </nav>
        <div class="container">
            <nav class="navbar navbar-default">
                <form action="VendorNearMe.php" method="post">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <label>Enter Area</label>&nbsp;
                                <select id="center" name="center" class="custom-select" onchange="getCenter()" required>
                                <option value="0" disable selected>Please Select</option>
                                <?php
                                        $option = '';
                                        $state = $con->query("SELECT DISTINCT CENTER_NAME FROM hawker");
                                            if($state->num_rows > 0){ 
                                                while($row = $state->fetch_assoc()){ ?>
                                                <option value = "<?php echo($row['CENTER_NAME'])?>">
                                                    <?php echo($row['CENTER_NAME'])?>
                                                </option><?php
                                                }
                                            }
                                            else{
                                                echo "NO DATA FOUND";
                                            }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <label>Enter Pincode</label>&nbsp;
                                <select id="pincode" name="pincode" class="custom-select" onchange="getCenter()">
                                    <option value="0" disable selected>Please Select</option>
                                    <?php
                                        $option = '';
                                        $state = $con->query("SELECT DISTINCT PINCODE FROM hawker");
                                            if($state->num_rows > 0){ 
                                                while($row = $state->fetch_assoc()){ ?>
                                                <option value = "<?php echo($row['PINCODE'])?>">
                                                    <?php echo($row['PINCODE'])?>
                                                </option><?php
                                                }
                                            }
                                            else{
                                                echo "NO DATA FOUND";
                                            }
                                    ?>
                                </select>
                            </div>
                        </div>
                        </form>
                </div>
            </nav>
            <script>
            </script>
            <h2>Results Showing for <span id="printCenter"><span><span id="printPincode"><span></h2>
            <table id="printTable" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>CENTER_NAME</th>
                        <th>HAWKER_NAME</th>
                        <th>ADDRESS</th>
                        <th>PINCODE</th>
                        <th>CONTACT_NO</th>
                    </tr>
                </thead>
                <tbody id="cardId">

                </tbody>
            </table>
        </div>
    </body>
</html>