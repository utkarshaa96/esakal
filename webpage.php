<?php
include('dbconfig.php');
?>
<!DOCTYPE html>
<html>
<!--  FileName : webpage.php
    Created By : Utkarsha Yewale
    Created Date : 24/09/2018 -->
<head>
    <title>eSakal | Subscription Page</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="webpage.css"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="http://www.esakal.com/sites/esakal/themes/smg980/favicon.ico" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="webpage.css">
    
    <!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" /> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <!-- <link rel="stylesheet" href="main.css">        -->
    <script>
    //code to get cities using ajax call
 function getCities() {
    var districtName = $("#districtId").val();
    //code for ajax call here will pass districtname to backend and get other details like cities
    $.ajax({
        type : "GET",
        url : "getCities.php?getDistrict="+districtName,
        success : function(result) {
            console.log(result);
            $("#cityId").html(result);
        },
        error : function() {
            return false;
        }
    })
}

// code to get pincode using ajax call
function getPincode(){
    var cityName = $("#cityId").val();
    //code for ajax call here will pass cityname to backend and get other details like pincode
    $.ajax({
        type : "GET",
        url : "getPincode.php?getCity="+cityName,
        success : function(result) {
            console.log(result);
            $("#pincodeId").html(result);  
        },
        error : function() {
            return false;
        }
    })
}
//end of ajax script

</script>
</head>
<body>
    <!-- Navbar Starts Here -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#" style="">
                    <img src="http://www.esakal.com/sites/esakal/themes/smg980/logo.png" width="200" height="50" class="d-inline-block align-top" alt="">
                </a>
            </div>
        </div>
    </nav>    
    <br><br>
    <div align="center">
        <h2>WELCOME TO SAKAL MEDIA GROUP</h2><br>
        <h3>SIGNUP TO SUBSCRIBE</h3>
    </div>
    <!-- Form starts here -->
    <div class="container-fluid">
        <form action="../backend/webpageDetails.php" class="needs-validation" method="post" id="form">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="fullname">Full Name</label>
                    <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Full Name" required>
                    <div class="invalid-feedback">
                        Please Enter Fullname    
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="Mobile Number">Mobile Number</label>
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" maxlength="10" required>
                    <div class="invalid-feedback">
                        Please Enter 10 digit mobile number   
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="Mobile Number">Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email@email.com" required>
                    <div class="invalid-feedback">
                        Please Enter Valid Email  
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="Address">Address</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="Full Address" required>
                    <div class="invalid-feedback">
                                Please Enter Full Address    
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="state">State</label>
                            <select id="state" name="state" class="custom-select" required>
                                <option value="0">Please Select</option>
                                <?php
                                    $option = '';
                                    $state = $con->query("SELECT DISTINCT state FROM aurangabad");
                                    if($state->num_rows > 0){ 
                                        while($row = $state->fetch_assoc()){ ?>
                                            <option value = "<?php echo($row['state'])?>">
                                                <?php echo($row['state'])?>
                                            </option><?php
                                            }
                                        }
                                        else{
                                            echo "NO DATA FOUND";
                                        }
                                        ?>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="district">District</label>
                        <select name="district" id="districtId" class="custom-select" required  onchange="getCities()">
                            <option  value="0">Please Select</option>
                            <?php
                                    $option = '';
                                    $district = $con->query("SELECT DISTINCT district FROM aurangabad WHERE state='MAHARASHTRA'");
                                        if($district->num_rows > 0){ 
                                            while($row = $district->fetch_assoc()){ ?>
                                            <option value = "<?php echo($row['district'])?>">
                                                    <?php echo($row['district'])?>
                                                </option><?php
                                            }
                                        }
                                        ?>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="city">City</label>
                        <select name="city" id ="cityId" class="custom-select" required onchange="getPincode()">
                            <option value="0">Please Select</option>
                        </select>
                    </div>
                <div class="form-group col-md-4">
                    <label for="pincode">PinCode</label>
                    <select id="pincodeId" name="pincode" class="custom-select" required>
                        <option value="0">Please Select</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="last">Last News Paper</label>
                    <input type="text" class="form-control" name="last" id="last" placeholder="Last News Paper">                    
                </div>
                <div class="form-group col-md-4">
                    <label for="start">Subscription Start-Date</label>
                        <input type="date" class="form-control" name="start" id="start" required>
                        <div class="invalid-feedback">
                            Please Enter Start date of Subscription    
                        </div>
                    </div>
                    <!-- Checkbox starts here -->
                    <div class="form-row col-md-4">
                        <label for="subscription">Subscription For</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="check[]" value="Sakal">
                            <label class="form-check-label" for="inlineCheckbox1" style="font-weight:normal;">Sakal</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="check[]" value="SakalTimes">
                            <label class="form-check-label" for="inlineCheckbox2" style="font-weight:normal;">SakalTimes</label>
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="check[]" value="Gomantak">
                            <label class="form-check-label" for="inlineCheckbox3" style="font-weight:normal;">Gomantak</label>
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox4" name="check[]" value="Agrowon">
                            <label class="form-check-label" for="inlineCheckbox4" style="font-weight:normal;">Agrowon</label>
                        </div>
                        
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox5" name="check[]" value="Saptahik">
                            <label class="form-check-label" for="inlineCheckbox5" style="font-weight:normal;">Saptahik</label>
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox6" name="check[]" value="Tanisha">
                            <label class="form-check-label" for="inlineCheckbox6" style="font-weight:normal;">Tanisha</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox7" name="check[]" value="Premier">
                            <label class="form-check-label" for="inlineCheckbox7" style="font-weight:normal;">Premier</label>
                        </div>
                    </div>
                </div><br>
                <div class="form-row" style="justify-content:center">
                    <button type="submit" class="btn btn-primary" data-toggle="modal" id="subscribe" onclick="check();"> Subscribe </button>&nbsp;&nbsp;
                    <button id="cancel" type="submit" class="btn btn-danger"> Cancel </button>
                </div>
            </form>
            <!-- Modal Starts Here -->
            <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="background:#004b8c;color:white;">
                        <h4 class="modal-title">eSakal </h4>
                        <button type7="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        ThankYou for Subscribing....
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>