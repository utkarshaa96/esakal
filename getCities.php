<?php
// File Name: getCities.php
// Created Date: 11/10/2018
// Created By: Utkarsha Yewale

// Return results of getCity AJAX Call for webpage

// Connection file
include('dbconfig.php');

//api for sending cities
if(isset($_GET['getDistrict'])){
    $district = $_GET['getDistrict'];
    // echo "----".$district;
	$result = "";
	$city = $con->query("SELECT DISTINCT city FROM aurangabad WHERE district='$district'");
	if($city->num_rows > 0){ 
		$newCity =  "<option value =''>Please Select</option>";
		$result = "$result $newCity";
		while($row = $city->fetch_assoc()){ 
			$newCity =  "<option value = ".$row['city'].">".$row['city']."</option>";
			$result = "$result $newCity";
		}
	}
	echo $result;
}