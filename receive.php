<?php
// Filename : receive.php
// Created on: 21-9-2018 
// Created by: Aarti

try{
        //Handles API Post requests
        if($_SERVER["REQUEST_METHOD"]==="POST"){
                $json = file_get_contents('php://input');
                if($json !== false){
                $data = json_decode($json, true);
                if ($data !== null){  
                        $contactNo=$data["who"];
                        $message=$data["what"];
                        $pincode=trim($message,"SUB");
                        $operator=$data["operator"];
                        $circle=$data["circle"];

                        if(isset($data["dateTime"])){
                                echo $data["dateTime"];
                                $timestamp=$data["dateTime"];
                        }
                        else{
                                date_default_timezone_set('Asia/Kolkata');
                                $timestamp=date('Y-m-d H:i:s');
                                echo $timestamp;
                        }
 
                        }
                }
        }
        //Handles API Get request      
        else if($_SERVER["REQUEST_METHOD"]==="GET"){
                if($_GET["who"] || $_GET["what"] || $_GET["operator"] || $_GET["circle"] || $_GET["qualityScore"] || $_GET["channelId"]){
                
                        $contactNo=$_GET["who"];
                        $message=$_GET["what"];
                        $pincode=trim($message,"SUB");
                        $operator=$_GET["operator"];
                        $circle=$_GET["circle"];

                        if(isset($_GET["dateTime"])){
                                echo '$data["dateTime"]';
                                $timestamp=$_GET["dateTime"];
                        }
                        else{
                                date_default_timezone_set('Asia/Kolkata');
                                $timestamp=date('Y-m-d H:i:s');
                                echo $timestamp;
                        }
        
                }
        }

        //$contactNo=$data["who"];
        if($contactNo!==null && $pincode!==null){
                $con=mysqli_connect("localhost","user","user@123","sakal");
                if(!$con){
                $log  = "SMS API - Message: DataBase connection failed ".
			"| IP Address: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a")."\n";
                logIt($log);
                        die("connection failed:".mysqli_connect_error());
                }
                else //echo"connection done";

                $status='pending';

                $sql="INSERT INTO list(contactNo,pincode,status,operator,circle,timesstamp) VALUES('$contactNo','$pincode','$status','$operator','$circle','$timestamp')";

                if ($con->query($sql) === TRUE){
                        $log  = "SMS API - Message: Success ".
				"| DateTime: ".$timestamp.
				"| ContactNo: ".$contactNo.
				"| IP Address: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").
				"| PinCode: ".$pincode."\n";
    		        throw new Exception($log);
                        $var=new \stdClass();
                        $var->Status_Code = "200 OK";
                        $var->Message="Record Added";
                        $myJSON = json_encode($var);
                        echo $myJSON;
                }

                else{
                        $log  = "SMS API - Message: Failed ".
				"| DateTime: ".$timestamp.
				"| ContactNo: ".$contactNo.
				"| IP Address: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").
				"| PinCode: ".$pincode."\n";
    		        throw new Exception($log);
                        $var=new \stdClass();
                        $var->Status_Code = "200 OK";
                        $var->Message="Record Added";
                        $myJSON = json_encode($var);
                        echo $myJSON;
                        echo "Error: " . $sql . "<br>" . $con->error;
                }

        $con->close();
        }
}
catch(Exception $e){
        logIt($log);
}

function logIt($log){
	echo $log;
    file_put_contents('./log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
}
?>

