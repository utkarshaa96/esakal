<?php
include('header.php');
?>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script  src="http://code.jquery.com/jquery-1.9.1.min.js" ></script>
    <script  src="function.js" ></script>
    <script src="https://www.w3schools.com/lib/w3.js"></script>
	<link rel="stylesheet" href="main.css">
    <title>eSakal | Vendor Status</title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
    <!-- Connected Starts Here-->
    <div class="container">
                <!-- Search Bar starts Here -->
            <div style="float:right;">
                <form action="excel.php?status=connected" method="post">
                    <button class="btn btn-success">Export
                        <input  type="hidden" name="x">
                    </button>
                </form>
            </div>
            <!-- <br> -->
            <input class="form-control" id="myInput" type="text" placeholder="Search.."><br>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="myTable2" class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                            <th>Vendor Status</th>
                            <th>Contact No.</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Pincode</th>
                            <th>Timestamp</th>
                            <th>Add Information</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            <?php
                            // $forVendor = 
                            if($forconnected!=null){
                                if($forconnected->num_rows > 0){ 
                                    while($row = $forconnected->fetch_assoc()){ ?>                
                                        <tr>
                                        <td><?php echo $row['vendorStatus']; ?></td>
                                        <td><?php echo $row['contactNo']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        <td><?php echo $row['address']; ?></td>
                                        <td><?php echo $row['city']; ?></td>
                                        <td><?php echo $row['pincode']; ?></td>
                                        <td><?php echo $row['timesstamp']; ?></td>
                                        <td><button class="btn btn-primary btn-sm"id="Add"onclick='showvendor(<?php echo $row["contactNo"]; echo ",";?>
                                                                                                            "<?php echo $row["name"];?>"
                                                                                                            <?php echo ","; echo $row["pincode"]; echo ",";?>
                                                                                                            "<?php echo $row["city"];?>"<?php echo",";?>
                                                                                                            "<?php echo $row["address"];?>"
                                                                                                            )'>Identify Vendor</button>&nbsp;&nbsp;
                                        </tr>
                                    <?php } 
                                }else{ ?>
                                <tr><td colspan="5">No details found.....</td></tr>
                                <?php }
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Popup for Add Vendor Starts Here -->
        <div align="center" class="block1 col-md-4" id="formdiv2" style="display:none;">
            <form action="connected.php" method="post">
                <input type="hidden" name="contactNo2" id="contact2" value="">
                <h5>Vendor's Information</h5>
                <div style="text-align:left;">
                <div class="dropdown" style="float:right;">
                    <select class="custom-select" required>
                        <option value="" disabled selected>Status</option>
                        <option value="Vendor Identified">Vendor Identified</option>
                        <option value="Vendor Contacted ">Vendor Contacted</option>
                        <option value="Subscription Started" >Subscription Started</option>
                        <option value="Subscription Not Started" >Subscription Not Started</option>
                    </select>
                </div><br>
                <div class="hide1" id="vendorNameDiv" name="vendorNameDiv">
                <b>Vendor's Name</b><span class="must">*</span>
                    <select class="custom-select">
                        <option value="" disable selected>Select Vendor</option>
                    </select>
                    <div>
                        <p id="name"></p>
                        <p id="contact"></p>
                        <p id="city"></p>
                        <p id="address"></p>
                        <p id="pincode"></p>
                    </div>
                </div>
                <!-- <input type="text" name="vendorName" id="vendorName" placeholder="Enter Vendor's Name" class="form-control"> -->
                <br><button class="btn btn-primary btn-sm" type="submit" id="submit">Submit</button>
                <button class="btn btn-primary btn-sm" type="button" onclick="location.href='connected.php'">Cancel</button>
            </form>
        </div>
</body>
</html>